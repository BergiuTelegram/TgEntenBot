# TelegramEntenBot
A Telegram bot that acts like a duck. An Example is running here [@enten2_bot](https://t.me/enten2_bot).

This Project is moved to [TgTriggerBots](https://gitlab.com/BergiuTelegram/TgTriggerBots).

## Dependencies
```
sudo apt install openjdk-8-jdk
```

## Installation
```shell
git clone https://gitlab.com/BergiuTelegram/TgEntenBot && cd TgEntenBot
./config.sh
./build
```

## Configuration
- Create a new TelegramBot with the [Botfather](https://telegram.me/botfather)
- Disable privacy settings for the bot
- Write your Botname into the file `BOTNAME` and your Token into the file `TOKEN`

## Run
- `./run`
